<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login</title>
  <link rel="icon" type="image/png" href="{{asset('/images/fav-icon.png')}}">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>
<style>
  body{
    background-size:cover;
    background-repeat:no-repeat;
  }
</style>
<body class="hold-transition login-page" background="images/wave.svg">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-warning">
    <div class="card-header text-center">
      <a href="#" class="h1"><small><b>Sistem</b><br>Pencatatan Keuangan</small></a>
    </div>
    <div class="card-body">
      @if (\Session::has('error'))
        <div class="alert alert-danger">
          {!! \Session::get('error') !!}
        </div>
      @elseif(\Session::has('success-logout'))
      <div class="alert alert-success">
        {!! \Session::get('success-logout') !!}
      </div>
      @endif
      <p class="login-box-msg">Sign in to start your session</p>
        @if ($errors->has('username'))
          @foreach ($errors->get('username') as $error)
            <i style="color:red"><small>{{ $error }}</small></i><br>
          @endforeach
        @endif
      <form method="POST" action="{{ route('login') }}" >
        @csrf
        <div class="input-group mb-3">
          
          <input type="email" name="email" class="form-control" placeholder="Email" required="required" autocomplete="off">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        
        @if ($errors->has('password'))
          @foreach ($errors->get('password') as $error)
            <i style="color:red"><small>{{ $error }}</small></i><br>
          @endforeach
        @endif
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password" required="required" autocomplete="off">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-warning btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <br>
      <p class="mb-0">
        <a href="register.html" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
</body>
</html>
