<!-- Brand Logo -->
@php
    $user = Auth::user();
@endphp
<a href="/sales" class="brand-link">
      <img src="{{asset('images/fav-icon.png')}}" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">WeeStore Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- SidebarSearch Form -->
      {{-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> --}}

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @if ($user->role == 1)
          <li class="nav-item">
            <a href="/sales" class="nav-link">
              <i class="nav-icon fa fa-cart-plus"></i>
              <p>
                Kelola Pesanan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Kelola Barang
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Kelola Pelanggan
              </p>
            </a>
          </li>
        @endif
        @if ($user->role == 2)
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-file-contract"></i>
              <p>
                Kelola Laporan
              </p>
            </a>
          </li>
        @endif
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->