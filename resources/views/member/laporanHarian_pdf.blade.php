<!DOCTYPE html>
<html>
<head>
	<title>Laporan Harian</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  padding: 5px;
  margin-left:80px;
  text-align:center;
}
</style>
<body>
    @php $user = Auth::user(); @endphp
	<center>
		<h2>Laporan Keuangan Harian {{$user->name}}</h2>
	</center>
 
	<table>
        <thead>
            <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Pemasukan</th>
                <th>Pengeluaran</th>
                <th>Saldo</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        @php $i=1; @endphp
        <tbody>
            @if($laporanAll != null)
                @foreach($laporanAll as $sld)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{date('d F Y', strtotime($sld->created_at))}}</td>
                    @if($sld->cat == 1)
                        <td>Rp {{ number_format($sld->jumlah, 0, '', '.') }}</td>
                    @else
                        <td>-</td>
                    @endif
                    @if($sld->cat == 2)
                        <td>Rp {{ number_format($sld->jumlah, 0, '', '.') ? number_format($sld->jumlah, 0, '', '.') : '-' }}</td>
                    @else
                        <td>-</td>
                    @endif
                    <td>{{$sld->saldo ? $sld->saldo : '-'}}</td>
                    <td>{{$sld->cat_name ? $sld->cat_name : '-'}}</td>
                </tr>
                @endforeach
            @else
                <tr>
                    <td>Data Not Found!</td>
                </tr>
            @endif
        </tbody>
    </table>
</body>
</html>