@extends('member.layouts.master')
@section('content')
@php
    $user = Auth::user();
@endphp
<div class="row justify-content-center align-items-center">
    <div class="row">
        <div class="text-center mb-5">
            <h4><b><span class="text-warning">Selamat datang</span>&nbsp;{{$user->name}}</b></h4>
            <p class="text-center text-small">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab excepturi fugit nisi quaerat explicabo alias quia pariatur veniam ullam perferendis eveniet officia</p>
        </div>
    </div>
    <div class="col-lg-3 col-6">
        <!-- small card -->
        <div class="small-box">
            <div class="inner">
                <h4><b>Rp {{ number_format($pm_thismonth, 0, '', '.') }}</b>
                </h4>
                <p>Pemasukan bulan ini
                </p>
            </div>
            <div class="icon">
                <i class="fas fa-dollar-sign">
                </i>
            </div>
            <a href="{{ route('pemasukan') }}" class="small-box-footer bg-warning"> More info 
                <i class="fas fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small card -->
        <div class="small-box">
        <div class="inner">
            <h4><b>Rp {{ number_format($png_thismonth, 0, '', '.') }}</b>
            </h4>
            <p>Pengeluaran bulan ini
            </p>
        </div>
        <div class="icon">
            <i class="fas fa-dollar-sign" >
            </i>
        </div>
        <a href="{{ route('pengeluaran') }}" class="small-box-footer bg-warning">
            More info 
            <i class="fas fa-arrow-circle-right">
            </i>
        </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small card -->
        <div class="small-box">
        <div class="inner">
            <h4><b>Rp {{ number_format($saldo, 0, '', '.') }}</b>
            </h4>
            <p>Saldo
            </p>
        </div>
        <div class="icon">
            <i class="fas fa-balance-scale-left">
            </i>
        </div>
        <a href="#" class="small-box-footer bg-warning">
            More info 
            <i class="fas fa-arrow-circle-right">
            </i>
        </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small card -->
        <div class="small-box">
        <div class="inner">
            <h3>>
            </h3>
            <p>Laba Rugi
            </p>
        </div>
        <div class="icon">
            <i class="fas fa-balance-scale-left">
            </i>
        </div>
        <a href="#" class="small-box-footer bg-warning">
            More info 
            <i class="fas fa-arrow-circle-right">
            </i>
        </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small card -->
        <div class="small-box">
        <div class="inner">
            <h3>>
            </h3>
            <p>Laporan
            </p>
        </div>
        <div class="icon">
            <i class="fas fa-print">
            </i>
        </div>
        <a href="{{ route('laporanMember') }}" class="small-box-footer bg-primary">
            More info 
            <i class="fas fa-arrow-circle-right">
            </i>
        </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small card -->
        <div class="small-box">
            <div class="inner">
                <h3>></h3>
                <p>Profile</p>
            </div>
            <div class="icon">
                <i class="fas fa-user" ></i>
            </div>
            <a href="#" class="small-box-footer bg-primary">
                More info 
                <i class="fas fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
</div>
@endsection
