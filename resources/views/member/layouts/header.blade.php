    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <a href="{{ route('logout') }}" class="btn btn-app bg-warning"><i class="fas fa-user"></i> Sign out</a>
    </ul>