@extends('member.layouts.master')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="col-lg-6 ml-0 float-left">
      <div class="col-lg-4  mb-3">
        <button type="button" class="btn btn-block btn-outline-secondary" data-toggle="modal" data-target="#modal-default"><i class="fas fa-plus"></i>&nbsp;&nbsp;Pemasukan</button>
      </div>
    </div>
    <div class="col-lg-4 float-right">
      <button type="button" class="btn btn-block btn-success">Saldo : Rp {{ number_format($saldo, 0, '', '.') }}</button>
    </div>
  </div>
  <div class="col-12">
    <div class="card">
      <div class="card-header bg-warning">
        <h3 class="card-title">Data Pemasukan</h3>
        <div class="card-tools">
              <div class="btn-group">
                  <button type="button" class="btn btn-default">Show</button>
                  <button type="button" class="btn btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <div class="dropdown-menu" role="menu" style="">
                    <a class="dropdown-item" href="#">Today</a>
                    <a class="dropdown-item" href="#">This Month</a>
                    <a class="dropdown-item" href="#">All</a>
                  <div class="dropdown-divider"></div>
                  </div>
              </div>
          </div>
        </div>
              <!-- /.card-header -->
        <div class="card-body table-responsive p-0" style="height: 300px;">
          <table class="table table-head-fixed text-nowrap table-bordered">
            <thead>
              <tr>
                <th>Tanggal</th>
                <th>Nama Pemasukan</th>
                <th>Nominal</th>
                <th>Keterangan</th>
              </tr>
            </thead>
            @php $i = 1 @endphp
            <tbody>
            @if($pemasukan != null)
                @foreach($pemasukan as $pm)
                <tr>
                  <td>{{date('d F Y', strtotime($pm->created_at))}}</td>
                  <td>{{ $pm->pm_name ? $pm->pm_name : '-' }}</td>
                  <td>Rp {{ number_format($pm->pm_nominal, 0, '', '.') }}</td>
                  <td>{{ $pm->pm_keterangan ? $pm->pm_keterangan : '-' }}</td>
                </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="5" class="text-center">Data Not Found!</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
        <div class="card-footer text-right">
            <b>Total Pemasukan Hari ini : Rp {{ number_format($today, 0, '', '.') }}</b>
        </div>
              <!-- /.card-body -->
      </div>
            <!-- /.card -->
    </div>
    <div class="col-lg-2">
      <a href="{{ route('member') }}" type="button" class="btn btn-block btn-primary"><i class="fas fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
    </div>
   </div>
   <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Pemasukan</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="{{ route('pemasukan.store') }}" method="post">
            @csrf
            <div class="modal-body">
                <div class="form-group">
                  <label class="note-form-label">Pemasukan</label>
                  <input type="text" name="pm_name" class="form-control" placeholder="Example: Gaji">
                </div>
                <div class="form-group">
                  <label class="note-form-label">Nominal</label>
                  <input type="text" name="pm_nominal" class="form-control" placeholder="Example: 50000">
                </div>
                <div class="form-group">
                  <label class="note-form-label">Keterangan (optional)</label>
                  <input type="text" name="pm_keterangan" class="form-control">
                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary">
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
</div>

@endsection