@extends('member.layouts.master')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Laporan Keuangan</h3>

                <div class="card-tools">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default">Cetak Laporan</button>
                        <button type="button" class="btn btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu" role="menu" style="">
                            <a class="dropdown-item" href="{{ route('laporanMember.laporanHarian') }}">Harian</a>
                            <a class="dropdown-item" href="#">Bulanan</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Pemasukan</th>
                            <th>Pengeluaran</th>
                            <th>Saldo</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    @php $i=1; @endphp
                    <tbody>
                        @if($saldo != null)
                            @foreach($saldo as $sld)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{date('d F Y', strtotime($sld->created_at))}}</td>
                                @if($sld->cat == 1)
                                    <td>Rp {{ number_format($sld->jumlah, 0, '', '.') }}</td>
                                @else
                                    <td>-</td>
                                @endif
                                @if($sld->cat == 2)
                                    <td>Rp {{ number_format($sld->jumlah, 0, '', '.') ? number_format($sld->jumlah, 0, '', '.') : '-' }}</td>
                                @else
                                    <td>-</td>
                                @endif
                                <td>Rp {{ number_format($sld->saldo, 0, '', '.') ? number_format($sld->saldo, 0, '', '.') : '-' }}</td>
                                <td>{{$sld->cat_name ? $sld->cat_name : '-'}}</td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>Data Not Found!</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
      <!-- /.card -->
    </div>
</div>
@endsection