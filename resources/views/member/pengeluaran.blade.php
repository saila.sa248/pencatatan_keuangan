@extends('member.layouts.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-6 float-left">
            <div class="col-lg-6 mb-3 float-left">
              <button type="button" class="btn btn-block btn-outline-secondary" data-toggle="modal" data-target="#modal-fixed"><i class="fas fa-plus"></i>&nbsp;&nbsp;pengeluaran tetap</button>
            </div>
            <div class="col-lg-6 mb-3 float-right">
                <button type="button" class="btn btn-block btn-outline-secondary" data-toggle="modal" data-target="#modal-var"><i class="fas fa-plus"></i>&nbsp;&nbsp;pengeluaran tak tetap</button>
            </div>
        </div>
        <div class="col-lg-4 float-right">
            <button type="button" class="btn btn-block btn-success">Saldo : Rp {{ number_format($saldo, 0, '', '.') }}</button>
        </div>
    </div>
  <div class="col-12">
    <div class="card">
      <div class="card-header bg-warning">
        <h3 class="card-title">Data pengeluaran</h3>
        <div class="card-tools">
              <div class="btn-group">
                  <button type="button" class="btn btn-default">Show</button>
                  <button type="button" class="btn btn-default dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <div class="dropdown-menu" role="menu" style="">
                    <a class="dropdown-item" href="#">Today</a>
                    <a class="dropdown-item" href="#">This Month</a>
                    <a class="dropdown-item" href="#">All</a>
                  <div class="dropdown-divider"></div>
                  </div>
              </div>
          </div>
        </div>
              <!-- /.card-header -->
        <div class="card-body table-responsive p-0" style="height: 300px;">
          <table class="table table-head-fixed text-nowrap table-bordered">
            <thead>
              <tr>
                <th>Tanggal</th>
                <th>Nama pengeluaran</th>
                <th>Nominal</th>
                <th>Kategori</th>
              </tr>
            </thead>
            @php $i = 1 @endphp
            <tbody>
            @if($pengeluaran != null)
                @foreach($pengeluaran as $png)
                <tr>
                    <td>{{date('d F Y', strtotime($png->created_at))}}</td>
                    <td>{{ $png->png_name ? $png->png_name : '-' }}</td>
                    <td>Rp {{ number_format($png->png_nominal, 0, '', '.') }}</td>
                    @if($png->png_cat == 1)
                        <td>Tetap</td>
                    @else
                        <td>Tidak Tetap</td>
                    @endif
                </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="5" class="text-center">Data Not Found!</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
        <div class="card-footer text-right">
            <b>Total pengeluaran Hari ini: Rp {{ number_format($today, 0, '', '.') }}</b>
        </div>
              <!-- /.card-body -->
      </div>
            <!-- /.card -->
    </div>
    <div class="col-lg-2">
      <a href="{{ route('member') }}" type="button" class="btn btn-block btn-primary"><i class="fas fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
    </div>
   </div>


    <div class="modal fade" id="modal-fixed">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah pengeluaran Tetap</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <small class="m-3"><i class="fas fa-exclamation-circle text-danger"></i>&nbsp;Pengeluaran tetap adalah biaya yang dikeluarkan secara tetap perbulan selama kurun waktu tertentu></small>
            
                <form action="{{ route('pengeluaran.fixedcost') }}" method="post">
                @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="note-form-label">pengeluaran</label>
                            <input type="text" name="png_name" class="form-control" placeholder="Example: Sewa Kosan">
                        </div>
                        <div class="form-group">
                            <label class="note-form-label">Nominal</label>
                            <input type="text" name="png_nominal" class="form-control" placeholder="Example: 50000">
                        </div>
                        <div class="form-group">
                            <label class="note-form-label">Keterangan </label>(optional)
                            <input type="text" name="png_keterangan" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-var">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah pengeluaran</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <small class="m-3"><i class="fas fa-exclamation-circle text-danger"></i>&nbsp;Pengeluaran tidak tetap adalah biaya yang dikeluarkan secara berubah ubah dan tak ada perencanaan sebelumnya</small>
            <!-- <div id="form_fixedcat" style="display:none"> -->
                <form action="{{ route('pengeluaran.varcost') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                    <label class="note-form-label">pengeluaran</label>
                    <input type="text" name="png_name" class="form-control" placeholder="Example: Traktir Teman">
                    </div>
                    <div class="form-group">
                    <label class="note-form-label">Nominal</label>
                    <input type="text" name="png_nominal" class="form-control" placeholder="Example: 50000">
                    </div>
                    <div class="form-group">
                    <label class="note-form-label">Keterangan </label>(optional)
                    <input type="text" name="png_keterangan" class="form-control">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary">
                </div>
                </form>
            <!-- </div> -->
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>

@endsection
