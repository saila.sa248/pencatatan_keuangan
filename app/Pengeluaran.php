<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengeluaran extends Model
{
    protected $table = 'pengeluaran';
    protected $primaryKey = 'png_id';

    public function users(){
        return $this->hasMany('App\User', 'user_id');
    }
}
