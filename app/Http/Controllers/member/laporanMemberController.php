<?php

namespace App\Http\Controllers\member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Saldo;
use Illuminate\Support\Facades\Auth;
use PDF;
use DB;

class laporanMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $saldo = Saldo::where('rec_creator', $user->id)->get();
        // $cat1 = Saldo::where('rec_creator', $user->id)->where('cat',1)->select(DB::raw('convert(created_at, date) as day'),DB::raw('sum(jumlah) as total'))->groupBy('day')->get();
        // $cat2 = Saldo::where('rec_creator', $user->id)->where('cat',2)->select(DB::raw('convert(created_at, date) as day'),DB::raw('sum(jumlah) as total'))->groupBy('day')->get();
    
        return view('member.Laporan', [
            'saldo' => $saldo,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function laporanHarian(){
        $user = Auth::user();

        $laporanAll = Saldo::where('rec_creator', $user->id)->select(DB::raw('convert(created_at, date) as day'), DB::raw('sum(jumlah) as total'))->groupBy('day')->get();
        
        $pdf = PDF::loadview('member.laporanHarian_pdf',['laporanAll'=>$laporanAll]);
    	return $pdf->download('laporan-harian.pdf');
    }
}
