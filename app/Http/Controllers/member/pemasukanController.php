<?php

namespace App\Http\Controllers\member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pemasukan;
use App\Saldo;
use Illuminate\Support\Facades\Auth;
use DB;
use Carbon\Carbon;

class pemasukanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $pemasukan = Pemasukan::where('rec_creator', $user->id)->orderBy('created_at', 'DESC')->get();
        $total_day = Pemasukan::where('rec_creator', $user->id)->select(DB::raw('convert(created_at, date) as day'), DB::raw('sum(pm_nominal) as total'))->groupBy('day')->get();
        $total_month = Pemasukan::where('rec_creator', $user->id)->select(DB::raw('MONTH(created_at) as months'), DB::raw('sum(pm_nominal) as total'))->groupBy('months')->get();
        // dd($total_month);
        $day = Pemasukan::where('rec_creator', $user->id)->select(DB::raw('sum(pm_nominal) as total'))->whereRaw('date(created_at) = CURDATE()')->groupBy('pm_id')->get();
        $today = $day->sum('total');

        $saldo_akhir = Saldo::latest('id')->first();
        if($saldo_akhir != null){
            $saldo = $saldo_akhir->saldo ? $saldo_akhir->saldo : '-';
        } else {
            $saldo = 0;
        }

        return view('member.pemasukan', [
            'pemasukan' => $pemasukan, 'saldo' => $saldo, 'total_day' => $total_day, 'today' => $today,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'pm_name' => 'required|max:255',
            'pm_nominal' => 'required|numeric',
            'pm_keterangan' => 'max:255',
        ],[
            'pm_name.required' => 'Nama Pemasukan harus diisi',
            'pm_name.max' => 'Nama Pemasukan Maksimal 255 Karakter',
            'pm_nominal.required' => 'Nominal harus diisi',
            'pm_nominal.numeric' => 'Nominal harus berupa angka',
            'pm_keterangan.max' => 'Keterangan maksimal 255 karakter',
        ]);
        
        $user = Auth::User();

        $pemasukan = new Pemasukan;
        $pemasukan->pm_name = $request->pm_name;
        $pemasukan->pm_nominal = $request->pm_nominal;
        $pemasukan->pm_keterangan = $request->pm_keterangan;
        $pemasukan->pm_status = 1;
        $pemasukan->pm_keterangan = $request->pm_keterangan;
        $pemasukan->rec_creator = $user->id;
        $pemasukan->save();

        $saldo_akhir = Saldo::latest('id')->first();

        if($saldo_akhir != null) {
            $saldo_akhir = $saldo_akhir->saldo;
        } else {
            $saldo_akhir = 0;
        }
        $total = $saldo_akhir + $request->pm_nominal;

        $saldo = new Saldo;
        $saldo->jumlah = $request->pm_nominal;
        $saldo->saldo = $total;
        $saldo->cat = 1;
        $saldo->rec_creator = $user->id;
        $saldo->cat_name = $request->pm_name;
        $saldo->save();

        return redirect()->back()->with('success', 'Pemasukan berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
