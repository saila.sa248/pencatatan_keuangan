<?php

namespace App\Http\Controllers\member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pengeluaran;
use App\Pemasukan;
use App\Saldo;
use Illuminate\Support\Facades\Auth;
use DB;

class pengeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $pengeluaran = Pengeluaran::where('rec_creator', $user->id)->orderBy('created_at', 'DESC')->get();
        $total = Pengeluaran::where('rec_creator', $user->id)->select(DB::raw('convert(created_at, date) as day'), DB::raw('sum(png_nominal) as total'))->groupBy('day')->get();
        $day = Pengeluaran::where('rec_creator', $user->id)->select(DB::raw('sum(png_nominal) as total'))->whereRaw('date(created_at) = CURDATE()')->groupBy('png_id')->get();
        $today = $day->sum('total');

        $saldo_akhir = Saldo::latest('id')->first();
        if($saldo_akhir != null){
            $saldo = $saldo_akhir->saldo ? $saldo_akhir->saldo : '-';
        } else {
            $saldo = 0;
        }

        return view('member.pengeluaran', ['pengeluaran' => $pengeluaran, 'saldo' => $saldo, 'today' => $today]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function fixedcost(Request $request)
    {
        $this->validate($request, [
            'png_name' => 'required|max:255',
            'png_nominal' => 'required|numeric',
            'png_keterangan' => 'max:255',
        ],[
            'png_name.required' => 'Nama kategori harus diisi',
            'png_name.max' => 'Nama kategori Maksimal 255 Karakter',
            'png_nominal.required' => 'Nominal harus diisi',
            'png_nominal.numeric' => 'Nominal harus berupa angka',
            'png_keterangan.max' => 'keterangan maksimal 255 karakter',
        ]);
        
        $user = Auth::User();

        $pengeluaran = new Pengeluaran;
        $pengeluaran->png_name = $request->png_name;
        $pengeluaran->png_nominal = $request->png_nominal;
        $pengeluaran->png_keterangan = $request->png_keterangan;
        $pengeluaran->png_status = 1;
        $pengeluaran->png_cat = 1;
        $pengeluaran->rec_creator = $user->id;
        $pengeluaran->save();

        $saldo_akhir = Saldo::latest('id')->first();
        $saldo_akhir = $saldo_akhir->saldo;
        $total = $saldo_akhir - $request->png_nominal;

        $saldo = new Saldo;
        $saldo->jumlah = $request->png_nominal;
        $saldo->saldo = $total;
        $saldo->cat = 2;
        $saldo->rec_creator = $user->id;
        $saldo->cat_name = $request->png_name;
        $saldo->save();

        return redirect()->back()->with('success', 'Pengeluaran berhasil ditambahkan!');
    }


    public function varcost(Request $request)
    {
        $this->validate($request, [
            'png_name' => 'required|max:255',
            'png_nominal' => 'required|numeric',
            'png_keterangan' => 'max:255',
        ],[
            'png_name.required' => 'Nama kategori harus diisi',
            'png_name.max' => 'Nama kategori Maksimal 255 Karakter',
            'png_nominal.required' => 'Nominal harus diisi',
            'png_nominal.numeric' => 'Nominal harus berupa angka',
            'png_keterangan.max' => 'keterangan maksimal 255 karakter',
        ]);
        
        $user = Auth::User();

        $pengeluaran = new Pengeluaran;
        $pengeluaran->png_name = $request->png_name;
        $pengeluaran->png_nominal = $request->png_nominal;
        $pengeluaran->png_keterangan = $request->png_keterangan;
        $pengeluaran->png_status = 1;
        $pengeluaran->png_cat = 2;
        $pengeluaran->rec_creator = $user->id;
        $pengeluaran->save();

        $saldo_akhir = Saldo::latest('id')->first();
        $saldo_akhir = $saldo_akhir->saldo;
        $total = $saldo_akhir - $request->png_nominal;

        $saldo = new Saldo;
        $saldo->jumlah = $request->png_nominal;
        $saldo->saldo = $total;
        $saldo->cat = 2;
        $saldo->rec_creator = $user->id;
        $saldo->cat_name = $request->png_name;
        $saldo->save();

        return redirect()->back()->with('success', 'Pengeluaran berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
