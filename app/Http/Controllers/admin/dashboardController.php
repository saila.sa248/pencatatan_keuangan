<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pemasukan;
use App\Pengeluaran;
use App\Saldo;
use DB;
use Carbon\Carbon;

class dashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }
    public function member()
    {
        $user = Auth::user();

        $now = Carbon::now()->month;
        $pm_month = Pemasukan::where('rec_creator', $user->id)->select(DB::raw('sum(pm_nominal) as total'))->whereMonth('created_at' , Carbon::now()->month)->groupBy('pm_id')->get();
        $pm_thismonth = $pm_month->sum('total');

        $png_month = Pengeluaran::where('rec_creator', $user->id)->select(DB::raw('sum(png_nominal) as total'))->whereMonth('created_at' , Carbon::now()->month)->groupBy('png_id')->get();
        $png_thismonth = $png_month->sum('total');

        $saldo_akhir = Saldo::latest('id')->first();
        if($saldo_akhir != null){
            $saldo = $saldo_akhir->saldo ? $saldo_akhir->saldo : '-';
        } else {
            $saldo = 0;
        }

        return view('member.dashboard', [
            'pm_thismonth' => $pm_thismonth, 'saldo' => $saldo, 'png_thismonth' => $png_thismonth,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
