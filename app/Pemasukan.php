<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemasukan extends Model
{
    protected $table = "pemasukan";
    protected $primaryKey ='pm_id';

    public function users(){
        return $this->hasMany('App\User', 'user_id');
    }

}
