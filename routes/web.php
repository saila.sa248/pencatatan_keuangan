<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login');
// });
// Route::post('/signIn', 'loginController@register')->name('signIn');
Route::get('/', 'loginController@showFormLogin')->name('login');
Route::get('login', 'loginController@showFormLogin')->name('login');
Route::post('login', 'loginController@login');

Route::group(['middleware' => 'auth'], function () {

    Route::get('dashboard', 'admin\dashboardController@index')->name('dashboard');
    Route::get('member', 'admin\dashboardController@member')->name('member');
    Route::get('logout', 'loginController@logout')->name('logout');

    // Pemasukan
    Route::prefix('pemasukan')->group(function(){
        Route::get('/', [\App\Http\Controllers\member\pemasukanController::class, 'index'])->name('pemasukan');
        Route::post('/store', [\App\Http\Controllers\member\pemasukanController::class, 'store'])->name('pemasukan.store');
    });

    // Pengeluaran
    Route::prefix('pengeluaran')->group(function(){
        Route::get('/', [\App\Http\Controllers\member\pengeluaranController::class, 'index'])->name('pengeluaran');
        Route::post('/fixedcost', [\App\Http\Controllers\member\pengeluaranController::class, 'fixedcost'])->name('pengeluaran.fixedcost');
        Route::post('/varcost', [\App\Http\Controllers\member\pengeluaranController::class, 'varcost'])->name('pengeluaran.varcost');
    });

    // Laporan Member
    Route::prefix('laporanMember')->group(function(){
        Route::get('/', [\App\Http\Controllers\member\laporanMemberController::class, 'index'])->name('laporanMember');
        Route::get('/laporanHarian', [\App\Http\Controllers\member\laporanMemberController::class, 'laporanHarian'])->name('laporanMember.laporanHarian');
    });

});
