<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Pemasukan;
use App\Pengeluaran;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
            'role' => 1,
            'status' => 1,
        ]);

        User::create([
            'name' => 'user',
            'email' => 'user@mail.com',
            'password' => bcrypt('user'),
            'role' => 2,
            'status' => 1,
        ]);

        User::create([
            'name' => 'saila',
            'email' => 'saila@gmail.com',
            'password' => bcrypt('1231'),
            'role' => 2,
            'status' => 1,
        ]);
    }
}
