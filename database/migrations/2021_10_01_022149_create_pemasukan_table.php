<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemasukanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemasukan', function (Blueprint $table) {
            $table->Increments('pm_id');
            $table->string('pm_name');
            $table->integer('pm_nominal');
            $table->text('pm_keterangan')->nullable();
            $table->enum('pm_status', [1,2])->comment('1 active, 2 inactive');
            $table->integer('rec_creator');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemasukan');
    }
}
