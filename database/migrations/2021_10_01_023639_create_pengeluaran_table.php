<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengeluaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengeluaran', function (Blueprint $table) {
            $table->Increments('png_id');
            $table->string('png_name');
            $table->integer('png_nominal');
            $table->text('png_keterangan')->nullable();
            $table->enum('png_cat', [1,2])->comment('1 fc, 2 vc');
            $table->enum('png_status', [1,2])->comment('1 active, 2 inactive');
            $table->integer('rec_creator');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengeluaran');
    }
}
